# Monitor Testing

This document explains the basics of running our monitoring framework within 
its required ecosystem.

Make sure you follow these instructions in order validate that you have properly 
installed the tool and its environment.

## Environment 

The explanation here follows the code from the 

    <project_dir>/evaluation/setup.sh

To run the monitoring framework you need to make sure that you have installed 
following dependencies: 

1) Monpoly or DejaVu: These are the first-order monitors supported by our framework
   as submonitors
   
   Consult the instructions for the individual tools at their respective websites.
   
   Monpoly: https://bitbucket.org/jshs/monpoly/src/master/
   
   DejaVu: https://github.com/krledmno1/dejavu
   
   Make sure that the two binaries are available on the path.
   
   For the purposes of the integration tests Monpoly is sufficient.
     
2) Flink

    Download it from here: https://flink.apache.org/downloads.html
    
    Extract it in a folder of your choice. For instance in 
    
        <project_dir>/tmp
        
    Copy the flink configuration from 
    
        <project_dir>/evaluation/flink-conf.yaml
    
    into
    
        <flink_dir>/conf/flink-conf.yaml

3) Kafka

    Download it from here: https://kafka.apache.org/downloads
    
    Extract it (e.g. in the tmp folder)
    
    Copy the file 
    
        <project_dir>/evaluation/server.properties
        
    into
    
        <kafka_dir>/conf/server.properties

4) Zookeeper

    Download it from here: https://zookeeper.apache.org/releases.html 
    
    Extract it (e.g. in the tmp folder)
    
    Rename the file
     
        <zookeeper_dir>/conf/zoo_sample.cfg
         
    into
     
        <zookeeper_dir>/conf/zoo.cfg 

    
## Running the integration tests

Before running any of the tests start the Flink cluster:

<flink_dir>/bin/start-cluster.sh

1) To run the simple test with a Replayer as the input source invoke 

./replayer.sh <flink_dir>

Replayer is good for a simple test without checkpointing or injected faults.
As it cannot rewind the trace and replay it since the last checkpoint (like
Kafka).


2) To run the test with a Kafka instance as the input source first you have to 
start Zookeeper and Kafka instances (in addition to the Flink cluster). To do that
invoke 

<flink_dir>/bin/zkServer.sh start

and 

<kafka_dir>/bin/kafka-server-start.sh <kafka_dir>/config/server.properties

(you may want to do the previous command in a separate terminal window)

The run the test with Kafka

./kafka.sh <flink_dir> <kafka_dir>

3) 


 