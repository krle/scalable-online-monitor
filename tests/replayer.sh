#!/bin/bash

# INPUT: flink (which is assumed configured) directory

PROCESSORS=1

WORKDIR=`cd "$(dirname "$BASH_SOURCE")/.."; pwd`
JARPATH="$WORKDIR/flink-monitor/target/flink-monitor-1.0-SNAPSHOT.jar"
DATADIR="$WORKDIR/evaluation/synthetic"
FORMULA="$DATADIR/triangle-neg.mfotl"

if [[ ! -z $2 ]]; then

    FORMULA=$2

fi

if [[ ! -r $JARPATH ]]; then
    echo "Error: Could not find monitor jar: $JARPATH"
    exit 2
fi

FLINKDIR="$1"
if [[ -z $FLINKDIR || ! -x $FLINKDIR/bin/flink ]]; then
    echo "Usage: $0 <path to Flink> [<path to a formula>]"
    echo "Make sure that the scripts in the <path to Flink>/bin are executable"
    exit 2
fi

TEMPDIR="$(mktemp -d)"
trap 'rm -rf "$TEMPDIR"' EXIT

fail() {
    echo "=== Test failed ==="
    exit 1
}

echo "Testing formula: "
cat $FORMULA

echo "Generating log ..."
"$WORKDIR/generator.sh" -T -e 1000 -i 10 -x 1 60 > "$TEMPDIR/trace.csv" && \
        "$WORKDIR/replayer.sh" -i csv -f monpoly -a 0 "$TEMPDIR/trace.csv" > "$TEMPDIR/trace.log"
if [[ $? != 0 ]]; then
    fail
fi

echo "Creating reference output ..."
monpoly -sig "$DATADIR/synth.sig" -formula $FORMULA -log "$TEMPDIR/trace.log" -nonewlastts -no_rw > "$TEMPDIR/reference.txt"
if [[ $? != 0 ]]; then
    fail
fi

echo "Running Flink monitor ..."
printf '>end<\n' >> "$TEMPDIR/trace.csv"

"$WORKDIR/replayer.sh" -i csv -f csv -a 0 -o localhost:10101 "$TEMPDIR/trace.csv" > "$TEMPDIR/trace.log" &
"$FLINKDIR/bin/flink" run "$JARPATH" --in localhost:10101 --format csv \
        --sig "$DATADIR/synth.sig" --formula $FORMULA \
        --negate false --monitor monpoly --command "monpoly -nonewlastts" \
        --processors $PROCESSORS --multi 1 --skipreorder --nparts 1 \
        --out "$TEMPDIR/flink-out" --job "integration-test-$RANDOM"
if [[ $? != 0 ]]; then
    fail
fi
find "$TEMPDIR/flink-out" -type f -exec cat \{\} + > "$TEMPDIR/out.txt"

echo
if "$WORKDIR/tests/verdicts_diff.py" "$TEMPDIR/reference.txt" "$TEMPDIR/out.txt"; then
    echo "=== Test passed ==="
else
    fail
fi

