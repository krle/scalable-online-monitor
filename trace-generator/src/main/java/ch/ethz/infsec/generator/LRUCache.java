package ch.ethz.infsec.generator;

import java.util.*;

public class LRUCache<T> {
    private final int capacity;
    private final ArrayList<T> valueList;
    private final LinkedHashMap<T, Integer> valueMap;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.valueList = new ArrayList<>(capacity);
        this.valueMap = new LinkedHashMap<>(capacity, 0.75f, true);
    }

    public int size() {
        return valueList.size();
    }

    public T get(int i) {
        return valueList.get(i);
    }

    public void hit(T value) {
        Integer index = valueMap.get(value);
        // because valueMap is created with accessOrder = true, get() moves the value
        // to the back if it already exists
        if (index == null) {
            if (valueList.size() < capacity) {
                valueList.add(value);
                valueMap.put(value, valueList.size() - 1);
            } else {
                final Map.Entry<T, Integer> toEvict = valueMap.entrySet().iterator().next();
                valueList.set(toEvict.getValue(), value);
                valueMap.remove(toEvict.getKey());
                valueMap.put(value, toEvict.getValue());
            }
        }
    }
}
