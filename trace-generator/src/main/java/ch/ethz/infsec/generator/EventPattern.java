package ch.ethz.infsec.generator;

import ch.ethz.infsec.monitor.DataType;
import ch.ethz.infsec.monitor.Signature;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

interface EventPattern {

    default Signature getSignature() {
        return new Signature() {
            private static final long serialVersionUID = -3532148047980195129L;

            private final List<String> events = Arrays.asList(getBaseEvent(), getPositiveEvent(), getNegativeEvent());
            private final Map<String, List<DataType>> parameters =
                    events.stream().collect(Collectors.toMap(e -> e,
                            e -> Collections.nCopies(getArguments(e).size(), DataType.INTEGRAL)));

            @Override
            public List<String> getEvents() {
                return events;
            }

            @Override
            public int getArity(String e) {
                return parameters.get(e).size();
            }

            @Override
            public List<DataType> getTypes(String e) {
                return parameters.get(e);
            }

            @Override
            public String getString() {
                StringBuilder signature = new StringBuilder();
                for (String event : events) {
                    signature.append(event).append('(');
                    final int arity = getArguments(event).size();
                    for (int i = 0; i < arity; ++i) {
                        if (i > 0) {
                            signature.append(',');
                        }
                        signature.append("int");
                    }
                    signature.append(")\n");
                }
                return signature.toString();
            }
        };
    }

    List<String> getVariables();

    String getBaseEvent();

    String getPositiveEvent();

    String getNegativeEvent();

    List<String> getArguments(String event);
}
