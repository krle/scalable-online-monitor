package ch.ethz.infsec.generator;

import ch.ethz.infsec.monitor.DataType;
import ch.ethz.infsec.monitor.CustomSignature;
import org.apache.commons.math3.distribution.IntegerDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.random.RandomGenerator;

import java.util.List;
import java.util.function.Supplier;

final class SimpleEventGenerator extends AbstractEventGenerator {

    private final CustomSignature sig;
    private final IntegerDistribution eventDistribution;
    private final IntegerDistribution intDistribution;
    private IntegerDistribution lengthDistribution;
    private final LRUCache<Integer> intValues;
    private final LRUCache<String> stringValues;
    private final int domainSize;
    private final float newFraction;
    private final String formula;

    SimpleEventGenerator(RandomGenerator random, int eventRate, int indexRate, long firstTimestamp, CustomSignature s, int ds, float nf, int dom, String f) {
        super(random, eventRate, indexRate, firstTimestamp);
        this.sig = s;
        this.domainSize = ds;
        this.newFraction = nf;
        intValues = new LRUCache<>(domainSize);
        stringValues = new LRUCache<>(domainSize);
        eventDistribution = new UniformIntegerDistribution(random, 0, sig.getEvents().size()-1);
        intDistribution = new UniformIntegerDistribution(random, 0, dom);
        lengthDistribution = new UniformIntegerDistribution(random, 1, 30);
        this.formula = f;
    }

    public SimpleEventGenerator withStringLength(int lower, int upper) {
        lengthDistribution = new UniformIntegerDistribution(random, lower, upper);
        return this;
    }

    @Override
    void appendNextEvent(StringBuilder builder, long timestamp) {
        //Sample event name
        List<String> events = sig.getEvents();
        String sampledEvent = events.get(eventDistribution.sample());
        List<DataType> parameterTypes = sig.getTypes(sampledEvent);

        appendEventStart(builder, sampledEvent);
        for (int i = 0; i < parameterTypes.size(); ++i) {
            if (parameterTypes.get(i) == DataType.STRING) {
                appendAttribute(builder, "x" + i, sampleValue(stringValues, this::sampleFreshString));
            } else {
                appendAttribute(builder, "x" + i, sampleValue(intValues, intDistribution::sample));
            }
        }
    }

    private String sampleFreshString() {
        final int length = lengthDistribution.sample();
        final char[] chars = new char[length];
        for (int i = 0; i < length; ++i) {
            // only use characters in [0-9A-Za-z] because of the limitations of the CSV format
            int x = random.nextInt(10 + 26 + 26);
            if (x < 10) {
                chars[i] = (char)('0' + x);
            } else if (x < 10 + 26) {
                chars[i] = (char)('A' + (x - 10));
            } else {
                chars[i] = (char)('a' + (x - 10 - 26));
            }
        }
        return new String(chars);
    }

    private <T> T sampleValue(LRUCache<T> values, Supplier<T> sample){
        if (values.size() > domainSize/2){

            int dom = Math.max(values.size(), 1);
            int dist= Math.round(dom/(1-newFraction));

            int sampleInd = random.nextInt(dist);

            if (sampleInd < values.size()) {
                return values.get(sampleInd);
            }
        }

        T value = sample.get();
        values.hit(value);
        return value;

    }

    @Override
    void initialize() {

    }

    @Override
    String getSignature() {
        return sig.getString();
    }

    @Override
    String getFormula() {
        return formula;
    }

    public static SimpleEventGenerator getInstance(RandomGenerator random,
                                                     int eventRate,
                                                     int indexRate,
                                                     long firstTimestamp,
                                                     CustomSignature s,
                                                     int ds,
                                                     float nf,
                                                     int dom,
                                                     String f){
        return new SimpleEventGenerator(random, eventRate, indexRate, firstTimestamp, s, ds, nf, dom,f);
    }

}
