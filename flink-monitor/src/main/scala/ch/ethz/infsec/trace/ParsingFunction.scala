package ch.ethz.infsec.trace

import ch.ethz.infsec.monitor.Fact
import ch.ethz.infsec.trace.parser.TraceParser
import org.apache.flink.api.common.functions.RichFlatMapFunction
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.checkpoint.ListCheckpointed
import org.apache.flink.util.Collector

import java.util

class ParsingFunction(var parser: TraceParser) extends RichFlatMapFunction[String, Fact] with ListCheckpointed[TraceParser] {
  override def open(parameters: Configuration): Unit = {
    val context = getRuntimeContext
    parser.setTraceId(context.getIndexOfThisSubtask, context.getNumberOfParallelSubtasks)
  }

  override def flatMap(line: String, collector: Collector[Fact]): Unit =
    if (line.startsWith(">EOF<")) {
      parser.endOfInput(collector.collect(_))
      collector.collect(Fact.meta("EOF"))
    } else {
      parser.parseLine(collector.collect(_), line)
    }

  override def snapshotState(checkpointId: Long, timestamp: Long): util.List[TraceParser] =
    util.Collections.singletonList(parser)

  override def restoreState(state: util.List[TraceParser]): Unit = {
    assert(state.size() == 1)
    parser = state.get(0)
  }
}
