package ch.ethz.infsec
package monitor

class EchoDejavuProcess(override val command: Seq[String]) extends DejavuProcess(command) {
  override protected def parseResult(sink: Fact => Unit)(line: String): Boolean = {
    if (line.startsWith(DejavuProcess.SYNC)) {
      false
    } else {
      sink(Fact.make("", 0L, line))
      true
    }
  }
}
