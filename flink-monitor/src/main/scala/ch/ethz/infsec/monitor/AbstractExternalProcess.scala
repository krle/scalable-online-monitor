package ch.ethz.infsec.monitor

import java.io._
import java.lang.ProcessBuilder.Redirect

import org.slf4j.{Logger, LoggerFactory}

import scala.collection.JavaConverters

abstract class AbstractExternalProcess extends ExternalProcess {
  @transient protected var process: Process = _

  @transient protected var writer: BufferedWriter = _
  @transient protected var reader: BufferedReader = _
  @transient protected var error: BufferedReader = _

  @transient private var logger: Logger = _

  override var identifier: Option[String] = None

  protected def open(command: Seq[String]): Unit = {
    logger = LoggerFactory.getLogger(getClass)
    require(process == null)

    val instantiatedCommand = identifier match {
      case Some(id) => command.map(_.replaceAll("\\{ID\\}", id))
      case None => command
    }

    logger.info("Starting external process {}", instantiatedCommand)
    process = new ProcessBuilder(JavaConverters.seqAsJavaList(instantiatedCommand))
      .start()

    writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream))
    reader = new BufferedReader(new InputStreamReader(process.getInputStream))
    error = new BufferedReader(new InputStreamReader(process.getErrorStream))
  }

  override def shutdown(): Unit = writer.close()

  /**
   * Parses the output from the external process contained in the line string
   * into a Fact object and writes it into the provided sink.
   * It returns true if there more lines are expected to be parsed from the external process.
   * @param line - an output string from the external process
   * @param sink - a sink to write the parsed facts
   * @return Boolean for the client that indecates whether to continue reading from the external process
   */
  protected def parseResult(sink: Fact => Unit)(line: String): Boolean

  /**
   * Same as parseResult only used to parse the standard error stream of the external process
   */
  protected def parseError(sink: Fact => Unit)(line: String): Boolean = {
    sink(Fact.meta("error",line))
    false
  }


  override def readResults(sink: Fact => Unit): Unit = {
    read(reader,parseResult(sink))
  }

  override def readErrors(sink: Fact => Unit): Unit = {
    read(error,parseError(sink));
  }

  private def read(reader: BufferedReader, parser: String => Boolean): Unit = {
    var more = true
    do {
      val line = reader.readLine()
      if (line == null) {
        more = false
      } else {
        more = parser(line)
      }
    } while (more)
  }

  override def join(): Int = process.waitFor()

  override def dispose(): Unit = {
    if (process != null)
      process.destroy()
    process = null
    writer = null
    reader = null
  }

}
