package ch.ethz.infsec.tools;

import ch.ethz.infsec.monitor.Fact;
import ch.ethz.infsec.slicer.HypercubeSlicer;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.longs.*;
import it.unimi.dsi.fastutil.objects.ReferenceArrayList;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.util.Collector;
import scala.Int;
import scala.Tuple2;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class ReorderFunction extends RichFlatMapFunction<Tuple2<Int, Fact>, Fact> implements CheckpointedFunction, Serializable {
    private static final long serialVersionUID = -4697615865165935107L;

    private static final int INITIAL_BUFFER_CAPACITY = 1024;

    // collapse indices?
    private final boolean collapse;

    // progress of each source task
    private long[] maxOrderElem;

    // number of sources that have signalled a global synchronisation event
    private int signalledNewStrategy;
    private int signalledEOF;

    // index that the next latency marker should be associated with
    private long maxIndex;

    // reorder buffer mapping indices to facts
    private final Long2ReferenceMap<ReferenceArrayList<Fact>> buffer;

    // index set: ordered representation of factsOfIndex's key set
    private final LongHeapPriorityQueue indices;

    // slicer (for re-distributing state)
    private final HypercubeSlicer slicer;

    // state handles
    private transient ListState<Integer> signalState = null;
    private transient ListState<Long> maxOrderElemState = null;
    private transient ListState<Long2ReferenceMap<ReferenceArrayList<Fact>>> bufferState = null;

    //private transient PrintWriter debug = null;

    abstract protected boolean isOrderElement(Fact fact);

    abstract protected long extractIndex(Fact fact);

    abstract protected Fact makeTerminator(long index);

    protected ReorderFunction(boolean collapse, int numSources, HypercubeSlicer slicer) {
        this.collapse = collapse;

        maxOrderElem = new long[numSources];
        Arrays.fill(maxOrderElem, Long.MIN_VALUE);

        signalledEOF = 0;
        signalledNewStrategy = 0;

        maxIndex = Long.MIN_VALUE;
        buffer = new Long2ReferenceOpenHashMap<>(INITIAL_BUFFER_CAPACITY);
        indices = new LongHeapPriorityQueue(INITIAL_BUFFER_CAPACITY);

        this.slicer = slicer;
    }

    @Override
    public void snapshotState(FunctionSnapshotContext functionSnapshotContext) throws Exception {
        final int thisSubtask = getRuntimeContext().getIndexOfThisSubtask();

        signalState.clear();
        maxOrderElemState.clear();
        bufferState.clear();

        if (thisSubtask == 0) {
            // NOTE(JS): We assume that every reorder subtask has the same view on
            // these state components, hence we only store them in the first subtask.
            // This should be correct as they are updated by broadcast events, which
            // should be ordered consistently with Flink's internal checkpoint triggers.
            signalState.add(signalledNewStrategy);
            signalState.add(signalledEOF);
            for (long l: maxOrderElem) {
                maxOrderElemState.add(l);
            }
        }

        final int degree = slicer.degree();
        ArrayList<Long2ReferenceOpenHashMap<ReferenceArrayList<Fact>>> slicedBuffers = new ArrayList<>(degree);
        for (int i = 0; i < degree; ++i) {
            slicedBuffers.add(new Long2ReferenceOpenHashMap<>());
        }

        final ArrayList<Tuple2<Object, Fact>> slices = new ArrayList<>();
        final ListCollector<Tuple2<Object, Fact>> collector = new ListCollector<>(slices);
        for (Long2ReferenceMap.Entry<ReferenceArrayList<Fact>> e: buffer.long2ReferenceEntrySet()) {
            for (Fact f: e.getValue()) {
                slices.clear();
                slicer.processEvent(f, collector);
                for (Tuple2<Object, Fact> tup: slices) {
                    final Long2ReferenceMap<ReferenceArrayList<Fact>> map = slicedBuffers.get((Integer) tup._1);
                    ReferenceArrayList<Fact> list;
                    if ((list = map.get(e.getLongKey())) == null) {
                        list = new ReferenceArrayList<>();
                        map.put(e.getLongKey(), list);
                    }
                    list.add(tup._2);
                }
            }
        }

        for (Long2ReferenceOpenHashMap<ReferenceArrayList<Fact>> map: slicedBuffers)
            bufferState.add(map);
    }

    @Override
    public void initializeState(FunctionInitializationContext ctxt) throws Exception {
        /*{
            final int thisSubtask = getRuntimeContext().getIndexOfThisSubtask();
            debug = new PrintWriter(new FileWriter("debug_reorder_" + thisSubtask + ".log"));
            debug.format("initializing reorder function for subtask %d\n", thisSubtask);
        }*/

        final ListStateDescriptor<Integer> signalStateDesc =
                new ListStateDescriptor<>(
                        "signalState",
                        TypeInformation.of(new TypeHint<Integer>() { })
                );
        signalState = ctxt.getOperatorStateStore().getUnionListState(signalStateDesc);

        final ListStateDescriptor<Long> maxOrderElemStateDesc =
                new ListStateDescriptor<>(
                        "maxOrderElemState",
                        TypeInformation.of(new TypeHint<Long>() { })
                );
        maxOrderElemState = ctxt.getOperatorStateStore().getUnionListState(maxOrderElemStateDesc);

        final ListStateDescriptor<Long2ReferenceMap<ReferenceArrayList<Fact>>> bufferStateDesc =
                new ListStateDescriptor<>(
                        "bufferState",
                        TypeInformation.of(new TypeHint<Long2ReferenceMap<ReferenceArrayList<Fact>>>() { })
                );
        bufferState = ctxt.getOperatorStateStore().getListState(bufferStateDesc);

        if (ctxt.isRestored()) {
            IntArrayList restoredSignals = new IntArrayList();
            for (Integer i : signalState.get()) {
                restoredSignals.add(i.intValue());
            }
            assert restoredSignals.size() == 2;
            signalledNewStrategy = restoredSignals.getInt(0);
            signalledEOF = restoredSignals.getInt(1);

            LongArrayList restoredMaxOrderElems = new LongArrayList();
            for (Long l : maxOrderElemState.get()) {
                restoredMaxOrderElems.add(l.longValue());
            }
            maxOrderElem = restoredMaxOrderElems.toLongArray();

            buffer.clear();
            for (Long2ReferenceMap<ReferenceArrayList<Fact>> map : bufferState.get()) {
                for (Long2ReferenceMap.Entry<ReferenceArrayList<Fact>> e : map.long2ReferenceEntrySet()) {
                    final long key = e.getLongKey();
                    ReferenceArrayList<Fact> l;
                    if ((l = buffer.get(key)) == null)
                        buffer.put(key, e.getValue());
                    else
                        l.addAll(e.getValue());
                }
            }

            indices.clear();
            for (long index : buffer.keySet()) {
                indices.enqueue(index);
            }
            maxIndex = buffer.keySet().stream().mapToLong(x -> x).max().orElse(Long.MIN_VALUE);
        }
    }

    private ReferenceArrayList<Fact> bufferOfIndex(long index) {
        ReferenceArrayList<Fact> list;
        if ((list = buffer.get(index)) == null) {
            //debug.format("  inserting new index\n");
            list = new ReferenceArrayList<>();
            buffer.put(index, list);
            indices.enqueue(index);
        }
        return list;
    }

    private void insertFact(long index, Fact fact) {
        bufferOfIndex(index).add(fact);
    }

    private long getMaxAgreedIndex() {
        long min = Long.MAX_VALUE;
        for (long l : maxOrderElem) {
            if (l < min)
                min = l;
        }
        return min;
    }

    private boolean isNextIndexReady(long maxAgreedIndex) {
        if (collapse) {
            return !indices.isEmpty() && indices.firstLong() < maxAgreedIndex;
        } else {
            return !indices.isEmpty() && indices.firstLong() <= maxAgreedIndex;
        }
    }

    private void flushReady(Collector<Fact> out, long maxAgreedIndex) {
        //debug.format("flushing up to %d\n", maxAgreedIndex);
        while (isNextIndexReady(maxAgreedIndex)) {
            final long index = indices.dequeueLong();
            final ReferenceArrayList<Fact> facts = buffer.remove(index);
            //debug.format("  flushing index %d with %d facts\n", index, facts.size());
            for (Fact fact : facts) {
                out.collect(fact);
            }
            out.collect(makeTerminator(index));
        }
        //debug.format("  flushing done, next index is %d\n", indices.isEmpty() ? -1 : indices.firstLong());
    }

    public int getNumSources() {
        return maxOrderElem.length;
    }

    @Override
    public void flatMap(Tuple2<Int, Fact> value, Collector<Fact> out) {
        final int source = (Integer) ((Object) value._1);
        final Fact fact = value._2;
        //debug.format("from %d received %s\n", source, fact);

        if (isOrderElement(fact)) {
            final long index = extractIndex(fact);
            //debug.format("  order element, index: %d, previous max: %d\n", index, maxOrderElem[source]);
            bufferOfIndex(index);  // ensures that a terminator is emitted even if the index has no facts
            if (index > maxOrderElem[source]) {
                maxOrderElem[source] = index;
                flushReady(out, getMaxAgreedIndex());
            }
        } else if (fact.isMeta()) {
            switch (fact.getName()) {
                case "EOF":
                    if (++signalledEOF == getNumSources()) {
                        flushReady(out, Long.MAX_VALUE);
                        out.collect(fact);
                    }
                    break;
                case "set_slicer":
                    if (++signalledNewStrategy == getNumSources()) {
                        signalledNewStrategy = 0;
                        slicer.unstringify((String) fact.getArgument(0));
                        out.collect(fact);
                    }
                    break;
                case "LATENCY":
                    final long maxAgreed = getMaxAgreedIndex();
                    if (maxAgreed >= maxIndex) {
                        if (!buffer.isEmpty()) {
                            throw new RuntimeException("!buffer.isEmpty()");
                        }
                        out.collect(fact);
                    } else {
                        if (buffer.get(maxIndex) == null) {
                            throw new RuntimeException("buffer.get(latencyIndex) == null");
                        }
                        insertFact(maxIndex, fact);
                    }
                    break;
                case "START":  // fallthrough
                case "WATERMARK":
                    break;
                default:
                    throw new RuntimeException("Unsupported meta fact: " + fact.getName());
            }
        } else {
            final long index = extractIndex(fact);
            if (index > maxIndex)
                maxIndex = index;
            insertFact(index, fact);
        }
    }

    @Override
    public void close() throws Exception {
        /*if (debug != null) {
            debug.format("closing\n");
            debug.close();
        }*/
        super.close();
    }
}

class ReorderTotalOrderFunction extends ReorderFunction {
    private static final long serialVersionUID = 259343779552099171L;

    // FIXME(JS): Should be stored in state.
    private final Long2LongMap tpToTs = new Long2LongOpenHashMap();

    ReorderTotalOrderFunction(int numSources, HypercubeSlicer slicer) {
        super(false, numSources, slicer);
    }

    @Override
    protected boolean isOrderElement(Fact fact) {
        boolean isTerminator = fact.isTerminator();
        if (isTerminator) {
            long tp = fact.getTimepoint();
            long ts = fact.getTimestamp();
            tpToTs.put(tp, ts);
        }
        return isTerminator;
    }

    @Override
    protected long extractIndex(Fact fact) {
        return fact.getTimepoint();
    }

    @Override
    protected Fact makeTerminator(long index) {
        long ts = tpToTs.get(index);
        tpToTs.remove(index);
        return Fact.terminator(ts);
    }
}

class ReorderCollapsedWithWatermarksFunction extends ReorderFunction {
    private static final long serialVersionUID = -1783789180639820020L;

    ReorderCollapsedWithWatermarksFunction(int numSources, HypercubeSlicer slicer) {
        super(true, numSources, slicer);
    }

    @Override
    protected boolean isOrderElement(Fact fact) {
        return fact.isMeta() && fact.getName().equals("WATERMARK");
    }

    @Override
    protected long extractIndex(Fact fact) {
        if (isOrderElement(fact)) {
            return Long.parseLong((String) fact.getArgument(0));
        } else {
            return fact.getTimestamp();
        }
    }

    @Override
    protected Fact makeTerminator(long index) {
        return Fact.terminator(index);
    }
}

class ReorderCollapsedPerPartitionFunction extends ReorderFunction {
    private static final long serialVersionUID = 4685346571481437100L;

    ReorderCollapsedPerPartitionFunction(int numSources, HypercubeSlicer slicer) {
        super(true, numSources, slicer);
    }

    @Override
    protected boolean isOrderElement(Fact fact) {
        return fact.isTerminator();
    }

    @Override
    protected long extractIndex(Fact fact) {
        return fact.getTimestamp();
    }

    @Override
    protected Fact makeTerminator(long index) {
        return Fact.terminator(index);
    }
}
