package ch.ethz.infsec

import ch.ethz.infsec.analysis.TraceAnalysis
import ch.ethz.infsec.kafka.MonitorKafkaConfig
import ch.ethz.infsec.monitor._
import ch.ethz.infsec.policy.{Formula, Policy}
import ch.ethz.infsec.tools._
import ch.ethz.infsec.trace.parser.{Crv2014CsvParser, JsonTraceParser, MonpolyTraceParser, TraceParser}
import org.apache.flink.api.common.restartstrategy.RestartStrategies
import org.apache.flink.api.common.time.Time
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.contrib.streaming.state.RocksDBStateBackend
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.{CheckpointingMode, TimeCharacteristic}
import org.slf4j.LoggerFactory
import org.slf4j.helpers.MessageFormatter

import java.io._
import java.lang.ProcessBuilder.Redirect
import java.nio.file.{FileSystems, Files}
import java.util.concurrent.{Callable, Executors, TimeUnit}
import scala.collection.JavaConverters
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

object StreamMonitoring {

  private val logger = LoggerFactory.getLogger(StreamMonitoring.getClass)

  private val MONPOLY_CMD = "monpoly"
  private val ECHO_DEJAVU_CMD = "echo-dejavu"
  private val ECHO_MONPOLY_CMD = "echo-monpoly"
  private val DEJAVU_CMD = "dejavu"

  var jobName: String = ""

  var checkpointUri: String = ""
  var checkpointInterval: Int = 10000
  var restarts: Int = 0

  var in: Option[EndPoint] = _
  var out: Option[EndPoint] = _
  var inputFormat: TraceParser = _
  var inGlob: String = ""
  var kafkaTestFile: String = ""

  var processors: Int = 0

  var monitorCommand: String = ""
  var command: Seq[String] = Seq.empty
  var formulaFile: String = ""
  var signatureFile: String = ""
  var signature: Signature = _
  var initialStateFile: Option[String] = None
  var negate: Boolean = false

  var injectFault = -1
  var simulateKafkaProducer: Boolean = false
  var queueSize = 10000
  var inputParallelism = 1
  var sourceIndexArgument: Option[Int] = None
  var clearTopic = false
  var multiSourceVariant: MultiSourceVariant = TotalOrder()
  var kafkatopic: String = ""
  var kafkagroup: String = ""

  var formula: Formula = policy.False()

  private def fail(message: String, values: Object*): Nothing = {
    logger.error(message, values: _*)
    System.err.println("Error: " + MessageFormatter.arrayFormat(message, values.toArray).getMessage)
    sys.exit(1)
  }

  def parseSocketEndpoint(s: String): SocketEndpoint = {
    val parts = s.split(':')
    if (parts.length != 2) {
      fail("Socket endpoint \"{}\" must have format <address>:<port>", s)
    }
    SocketEndpoint(parts(0), parts(1).toInt)
  }

  def parseEndpointArg(ss: String): Option[EndPoint] = {
    /**
     * IP:Port        OR
     * kafka          OR
     * filename
     */
    if (ss.equalsIgnoreCase("kafka")) {
      return Some(KafkaEndpoint())
    }
    if (ss.startsWith("socket:")) {
      return Some(parseSocketEndpoint(ss.stripPrefix("socket:")))
    }
    if (ss.startsWith("file:")) {
      return Some(FileEndPoint(ss.stripPrefix("file:")))
    }
    try {
      if (ss.isEmpty) None
      else {
        val s = ss.split(":")
        if (s.length > 1) {
          val p = s(1).toInt
          Some(SocketEndpoint(s(0), p))
        } else {
          Some(FileEndPoint(ss))
        }
      }
    }
    catch {
      case _: Exception => None
    }
  }

  def loadCustomTraceParser(className: String): TraceParser = {
    val parserClass = try {
      getClass.getClassLoader.loadClass(className)
    } catch {
      case _: ClassNotFoundException => fail("Could not load trace parser class " + className)
    }
    parserClass.getConstructor().newInstance().asInstanceOf[TraceParser]
  }

  def init(params: ParameterTool) {
    clearTopic = params.getBoolean("clear", false)
    inputParallelism = params.getInt("nparts", 1)
    sourceIndexArgument = {
      val i = params.getInt("part-index", -1)
      if (0 <= i) Some(i) else None
    }
    jobName = params.get("job", "Parallel Online Monitor")
    kafkaTestFile = params.get("kafkatestfile", "")

    checkpointUri = params.get("checkpoints", checkpointUri)
    checkpointInterval = params.getInt("checkpoint-interval", checkpointInterval)
    restarts = params.getInt("restarts", restarts)

    in = parseEndpointArg(params.get("in", "127.0.0.1:9000"))
    out = parseEndpointArg(params.get("out", ""))

    signatureFile = params.get("sig")
    signature = CustomSignature.parse(signatureFile)

    inputFormat = params.get("format", "monpoly") match {
      case "monpoly" => new MonpolyTraceParser(signature)
      case "csv" => new Crv2014CsvParser(signature)
      case "json" =>
        // TODO(JS): The JSON parser has a fixed signature.
        // It would make sense to write it to a temporary file and use it to invoke the monitor process.
        val timestampPath = params.get("json-timestamp", "")
        new JsonTraceParser(JavaConverters.seqAsJavaList(timestampPath.split('.')))
      //case "dejavu" => DejavuFormat
      case name => loadCustomTraceParser(name)
    }

    inGlob = params.get("glob", "")

    processors = params.getInt("processors", 1)
    logger.info(s"Using $processors parallel monitors")

    negate = params.getBoolean("negate", false)
    monitorCommand = params.get("monitor", MONPOLY_CMD)
    multiSourceVariant = params.getInt("multi", 1) match {
      case 1 => TotalOrder()
      case 2 => PerPartitionOrder()
      case 3 => WaterMarkOrder()
      case 4 => WaterMarkOrder()
      case _ => fail("multisourcevariant parameter must be between 1 and 4")
    }

    val commandString = params.get("command")
    command = if (commandString == null) Seq.empty else commandString.split(' ')
    formulaFile = params.get("formula")
    val formulaSource = Source.fromFile(formulaFile).mkString
    formula = Policy.read(formulaSource) match {
      case Left(err) => fail("Cannot parse the formula: " + err)
      case Right(phi) => phi
    }

    initialStateFile = Option(params.get("load"))

    queueSize = params.getInt("queueSize", queueSize)
    injectFault = params.getInt("inject-fault", injectFault)

    kafkatopic = params.get("kafka-topic", "monitor_topic")
    kafkagroup = params.get("kafka-group", "monitor")
  }


//  def calculateSlicing(params: ParameterTool): Unit =
//  {
//    val degree = params.getInt("degree",4)
//    formulaFile = params.get("formula")
//    val formulaSource = Source.fromFile(formulaFile).mkString
//    formula = Policy.read(formulaSource) match {
//      case Left(err) =>
//        logger.error("Cannot parse the formula: " + err)
//        sys.exit(1)
//      case Right(phi) => phi
//    }
//    var dfms = new DeciderFlatMapSimple(degree,formula,Double.MaxValue)
//    val file = params.get("file")
//    var content = Source.fromFile(file).mkString.split("\n").map(_.trim)
//    val parser = MonpolyFormat.createParser()
//   // var arr = scala.collection.mutable.ArrayBuffer[Record]()
//    var arr = parser.processAll(content)
//    //explicit nulling to aid the JVM in GCing
//    content = null
//
//    val slicer = params.get("slicer",null)
//    var strategy : HypercubeSlicer = null
//    if(slicer == null) {
//      arr.foreach(x => {
//        if (!x.isEndMarker)
//          dfms.windowStatistics.addEvent(x)
//      })
//      dfms.windowStatistics.nextFrame()
//      strategy = dfms.getSlicingStrategy(dfms.windowStatistics)
//    }else{
//      val res = new SlicerParser().parseSlicer(slicer)
//      strategy = new HypercubeSlicer(formula,res._1,res._2,1234)
//      strategy.parseSlicer(slicer)
//    }
//    val strategyStr = strategy.stringify()
//    val writer = new java.io.PrintWriter("strategyOutput")
//    writer.println(strategyStr)
//    writer.close()
//    var outs = strategy.processAll(arr)
//    //explicit nulling to aid the JVM in GCing
//    arr = null
//    var sortedOuts = scala.collection.mutable.Map[Int,scala.collection.mutable.ArrayBuffer[Record]]()
//    outs.foreach(x => {
//        if(!sortedOuts.contains(x._1))
//          sortedOuts += ((x._1,scala.collection.mutable.ArrayBuffer[Record]()))
//        sortedOuts(x._1) += x._2
//    })
//    //explicit nulling to aid the JVM in GCing
//    outs = null
//    var printer = new KeyedMonpolyPrinter[Int, Nothing](false)
//    sortedOuts.foreach(x => {
//      val w2 = new java.io.PrintWriter("slicedOutput"+x._1)
//      x._2.foreach(rec => {
//        printer.process(Left((x._1,rec)), {case Left(y) => w2.println(y.in); case Right(_) => ()})
//      })
//      w2.close()
//    })
//    sortedOuts = null
//    val w3 = new PrintWriter("otherStuff")
//    w3.println("relationSet size: "+dfms.windowStatistics.relations.size)
//    w3.println("heavy hitter size: "+dfms.windowStatistics.heavyHitter.values.size)
//    w3.println("heavy hitters:")
//    dfms.windowStatistics.heavyHitter.foreach(x => {
//      w3.println(x)
//    })
//    w3.println("relation sizes:")
//    dfms.windowStatistics.relations.foreach(x => {
//      w3.println(x._1 + " - " + x._2 + "(relation) " + dfms.windowStatistics.relationSize(x._1) + "(relationSize)")
//    })
//    w3.close()
//  }
//
//  def exename(str: String):String = {
//    val ss = str.split('/')
//    ss(ss.length-1)
//  }



  def main(args: Array[String]) {
    val params = ParameterTool.fromArgs(args)

    val analysis = params.getBoolean("analysis", false)
//    val calcSlice = params.getBoolean("calcSlice",false)

    if (analysis) TraceAnalysis.prepareSimulation(params)
//    else if(calcSlice) calculateSlicing(params)
    else {
      init(params)

      //TODO: replace
      val slicer = SlicingSpecification.mkSlicer(params, formula, processors)

      val signatureMap = (JavaConverters.asScalaBuffer(signature.getEvents)
        .map(e => (e, signature.getArity(e)) -> JavaConverters.asScalaBuffer(signature.getTypes(e)))
        .toMap) ++
          Map(  // TODO(JS): Can we move this to a better place?
            ("tp", 1) -> Seq(DataType.INTEGRAL),
            ("ts", 1) -> Seq(DataType.INTEGRAL),
            ("tpts", 2) -> Seq(DataType.INTEGRAL, DataType.INTEGRAL))

      val freeVariableTypeMap = formula.freeVariableTypes(signatureMap)
      val freeVariableTypes = formula.freeVariablesInOrder.distinct.map(v => freeVariableTypeMap(v))

      val monitorProcess: ExternalProcess = monitorCommand match {
        case MONPOLY_CMD => {
          val monitorArgs = if (command.nonEmpty) command else (if (negate) monitorCommand :: List("-negate") else List(monitorCommand))
          val margs = monitorArgs ++ List("-sig", signatureFile, "-formula", formulaFile)
          logger.info("Monitor command: {}", margs.mkString(" "))
          new MonpolyProcess(margs, initialStateFile, inputParallelism, freeVariableTypes)
        }
        case ECHO_DEJAVU_CMD => {
          val monitorArgs = if (command.nonEmpty) command else List(monitorCommand)
          logger.info("Monitor command: {}", monitorArgs.mkString(" "))
          new EchoDejavuProcess(monitorArgs)
        }
        case ECHO_MONPOLY_CMD => {
          val monitorArgs = if (command.nonEmpty) command else List(monitorCommand)
          logger.info("Monitor command: {}", monitorArgs.mkString(" "))
          new EchoMonpolyProcess(monitorArgs, inputParallelism, freeVariableTypes)
        }
        case DEJAVU_CMD => {
          if (slicer.requiresFilter) {
            fail("Formulas containing equality or duplicate predicate names cannot be used with DejaVu")
          }

          val monitorArgs = if (command.nonEmpty) command else List(monitorCommand)
          val dejavuFormulaFile = formulaFile + ".qtl"
          val writer = new PrintWriter(new FileOutputStream(dejavuFormulaFile, false))
          writer.write(formula.toQTLString(!negate))
          writer.close()

          //Compiling the monitor
          val compileArgs = monitorArgs ++ List("compile", dejavuFormulaFile)
          val process = new ProcessBuilder(JavaConverters.seqAsJavaList(compileArgs))
            .redirectError(Redirect.INHERIT)
            .start()
          val reader = new BufferedReader(new InputStreamReader(process.getInputStream))

          val readerThread = new Callable[String] {
            override def call(): String = reader.readLine()
          }

          try {
            val executor = Executors.newFixedThreadPool(1)
            val monitorLocation = executor.submit(readerThread).get(20, TimeUnit.SECONDS)
            executor.shutdown()
            val runArgs = monitorArgs ++ List("run", monitorLocation, "25")
            logger.info("Monitor command: {}", runArgs.mkString(" "))
            new DejavuProcess(runArgs)
          } catch {
            case e: Exception => {
              fail("Dejavu monitor compilation failed: {}", e.getMessage)
            }
          }
        }
        case c@_ => fail("Cannot parse the monitor command: {}", c)
      }

      val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

      if (!checkpointUri.isEmpty) {
        env.setStateBackend(new RocksDBStateBackend(checkpointUri))
        env.enableCheckpointing(checkpointInterval, CheckpointingMode.EXACTLY_ONCE)
      }
      val restartStrategy = if (restarts <= 0) {
        RestartStrategies.noRestart()
      } else {
        RestartStrategies.fixedDelayRestart(restarts, Time.of(1, TimeUnit.SECONDS))
      }
      env.setRestartStrategy(restartStrategy)

      // Disable automatic latency tracking, since we inject latency markers ourselves.
      env.getConfig.setLatencyTrackingInterval(-1)

      // Performance tuning
      env.getConfig.enableObjectReuse()
      env.getConfig.registerTypeWithKryoSerializer(classOf[Fact], new FactSerializer)

      env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

      var monitor: StreamMonitorBuilder = null
      // textStream = env.addSource[String](helpers.createStringConsumerForTopic("flink_input","localhost:9092","flink"))
      val textStream = in match {
        case Some(SocketEndpoint(h, p)) =>
          monitor = multiSourceVariant.getStreamMonitorBuilder(env, inputParallelism, slicer)
          Thread.sleep(4000)
          monitor.socketSource(h, p)
        case Some(FileEndPoint(path)) =>
          val paths: ArrayBuffer[String] = new ArrayBuffer[String]()
          if (inGlob.isEmpty) {
            logger.info("Reading trace from single file: {}", path)
            paths += path
          } else {
            logger.info("Reading trace from directory: {}", path)
            val stream = Files.newDirectoryStream(FileSystems.getDefault.getPath(path), inGlob)
            try {
              val it = stream.iterator()
              while (it.hasNext) paths += it.next().toString
            } finally stream.close()
            logger.info("Found {} trace files to read from", paths.length)
          }
          inputParallelism = paths.length
          monitor = multiSourceVariant.getStreamMonitorBuilder(env, inputParallelism, slicer)
          monitor.fileSource(paths)
        case Some(KafkaEndpoint()) =>
          val specifiedInputParallelism = if (inputParallelism >= 1) Some(inputParallelism) else None
          MonitorKafkaConfig.init(topicName = kafkatopic, groupName = kafkagroup, clearTopic = clearTopic, numPartitions = specifiedInputParallelism)
          inputParallelism = MonitorKafkaConfig.getNumPartitions
          if (!kafkaTestFile.isEmpty) {
            val PrefixRegex = """(.*)/(.*)""".r
            val PrefixRegex(kafkaDir, kafkaPrefix) = kafkaTestFile
            val testProducer = new KafkaTestProducer(kafkaDir, kafkaPrefix)
            testProducer.runProducer(true)
          }
          monitor = multiSourceVariant.getStreamMonitorBuilder(env, inputParallelism, slicer)
          monitor.kafkaSource()
        case _ => fail("Cannot parse the input argument")
      }
      env.setMaxParallelism(inputParallelism)
      env.setParallelism(inputParallelism)
      /*if (params.has("skipreorder") && inputParallelism != 1)
        fail("skipreorder ==> inputParallelism == 1")*/
      inputFormat.setTerminatorMode(multiSourceVariant.getTerminatorMode)
      val verdicts = monitor.assemble(textStream, inputFormat, params.has("decider"), !params.has("skipreorder"), slicer, monitorProcess, queueSize, params.getInt("windowsize",100), injectFault)

      out match {
        case Some(SocketEndpoint(h, p)) => monitor.socketSink(verdicts, h, p)
        case Some(FileEndPoint(f)) => monitor.fileSink(verdicts, f)
        case _ => monitor.printSink(verdicts)
      }


      if(params.has("decider")) {
        //val decider = new AllState(new DeciderFlatMapSimple(slicer.degree, formula, params.getInt("windowsize",100)))
        Rescaler.create(jobName, "127.0.0.1", processors)

        //new AllState(new DeciderFlatMapSimple(slicer.degree, formula, params.getInt("windowsize",100)))
        Thread.sleep(2000)
      }

      env.execute(jobName)
    }
  }
}
